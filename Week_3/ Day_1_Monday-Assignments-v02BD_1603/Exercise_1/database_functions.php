<?php


require_once ('generic_functions.php');


function databasePath($query){

	$databaseName = databaseName($query);
	$tablePath = "/home/".gethostname()."/bin/".$databaseName;
	return $tablePath;
}


function databaseName($query){

	$databaseName = get_string_between($query, "\"", "\"");
	return $databaseName;
}



function createDatabase($query){

	if(strpos($query, 'CREATE,DATABASE,') !== false){

		$databasePath = databasePath($query);
		mkdir($databasePath, 0700);
		$databaseName = databaseName($query);
		echo "\"".$databaseName."\""." CREATED"."\n";
	}
	
	
}


function deleteDatabase($query,$databasePath){

	if(strpos($query, "DELETE,DATABASE,") !== false){

		deleteDir($databasePath);
		$databaseName = databaseName($query);
		echo "\"".$databaseName."\""." DELETED"."\n";
	}
	
	
}


 
 function createTable($query,$tablePath){

	
	if(strpos($query, "CREATE,TABLE,") !== false){

		$tableName = databaseName($query);
		$tablePath = $tablePath."/".$tableName.".csv";
		$query = substr($query, strpos($query, ",COLUMNS,") + 1);	// taking all arguments before the word COLUMNS,
		 					
		 $result = array();  
		 preg_match_all('/".*?"|\'.*?\'/', $query, $result);	 // taking only the words between quotes
		 					
		 $result2 = array(); 
		 for($i=0 ; $i<count($result); $i++){
  		   	 $result2[$i]=str_replace("\"","",$result[$i]);	// removing the quotes from the taken arguments
		 }
		                    
		 $fp = fopen($tablePath, "w");
		 foreach($result2 as $column){
   			 fputcsv ($fp, $column, "\t");
		 }
		 fclose($fp);
		  
		 echo "\"".$tableName."\""." CREATED"."\n";
	}

}




function deleteRecord($query,$tablePath){

	
	if(strpos($query, "DELETE,ROW,") !== false){

		$result = array();
		preg_match_all('/".*?"|\'.*?\'/', $query, $result);		// taking only the words between quotes

		for($i=0 ; $i<count($result); $i++){
  			$result[$i]=str_replace("\"","",$result[$i]);	// removing the quotes from the taken arguments
		}

		$str="";
		foreach($result as $key => $val){
    		$str = join(",", $result[$key]);	// converting the taken input array to string
		}
		
		$file=file($tablePath);
		$list=array();
		
		foreach($file as $value){
    		if(stristr($value, $str) == false){
				$list[] = str_getcsv($value,"\t");
			}
		}
		
		$fp = fopen($tablePath, "w");
		foreach($list as $column){
   			fputcsv ($fp, $column, "\t");
		}
		fclose($fp);
		echo "Record DELETED"."\n";
	}

		
}




function addRecord($query,$tablePath){

	
	if(strpos($query, "ADD,") !== false){

		$result = array();
		preg_match_all('/".*?"|\'.*?\'/', $query, $result);
		
		$result2 = array();
		for($i=0 ; $i<count($result); $i++){
  			$result2[$i]=str_replace("\"","",$result[$i]);
		}

		$fp = fopen($tablePath, "a");
		foreach($result2 as $column){
   			fputcsv ($fp, $column, "\t");
		}
		fclose($fp);
		echo "Record ADDED"."\n";
	}

		
}




function retrieveRecord($query,$tablePath){

	if(strpos($query, "GET,") !== false){

		$result = array();
		preg_match_all('/".*?"|\'.*?\'/', $query, $result);
				
		$result2 = array();
		for($i=0 ; $i<count($result); $i++){
  			$result2[$i]=str_replace("\"","",$result[$i]);
		}
			
		$str="";
		foreach($result2 as $key => $val){
    		$str = join(",", $result2[$key]);
		}
			
		$file=file($tablePath);
		$list=array();
		
		foreach($file as $value){
    		if(stristr($value, $str)){
				$list[] = str_getcsv($value,"\t");
			}
		}
		
		foreach($list as $key => $val){
    		echo "\"".join("\",\"", $list[$key])."\"";
		}
		echo "\n";
	}

	
}




?>