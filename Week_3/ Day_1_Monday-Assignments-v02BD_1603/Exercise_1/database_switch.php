#!/usr/bin/php5



<?php


require_once ('generic_functions.php');
require_once ('table_functions.php');


class Database{

function manipulateDatabase($tablePath,$tableName){


	$tablePath = $tablePath;
	$tableName = $tableName;

	
	$input = readline();

	switch($input){

	    
	   case "-- Create a Database":
		   $query = readline();
		   createDatabase($query);
		   $tablePath = databasePath($query);
		   $tableName = databaseName($query);
		   $this ->  manipulateDatabase($tablePath,$tableName);
		break;

		
		case "-- Create a table":
			$query = readline();
			createTable($query,$tablePath);
			$tableName = databaseName($query);
			$this ->  manipulateDatabase($tablePath,$tableName);
 		break;


		case "-- Add a record":
			$query = readline();
 			$fullPath = $tablePath ."/".$tableName.".csv";
	  		addRecord($query,$fullPath);
	  		$this ->  manipulateDatabase($tablePath,$tableName);
		break;

 		
 		case "-- Retrieve a record":
			$query = readline();
			$fullPath = $tablePath ."/".$tableName.".csv";
  			retrieveRecord($query,$fullPath);
  			$this ->  manipulateDatabase($tablePath,$tableName);
 		break;

 		
 		case "-- Delete a record":
		 	$query = readline();
		 	$fullPath = $tablePath ."/".$tableName.".csv";
			deleteRecord($query,$fullPath);
			$this ->  manipulateDatabase($tablePath,$tableName);
		break;


		case "-- Delete a Database":
			$query = readline();
			$tablePath = databasePath($query);
			deleteDatabase($query,$tablePath);
			$tableName = databaseName($query);
			$this ->  manipulateDatabase($tablePath,$tableName);
		break;


		case "exit":
		break;

		

  		
	}
  }

}
		$tablePath = "";
		$tableName = "";
		$database = new Database;
		$database->manipulateDatabase($tablePath,$tableName);

?>






